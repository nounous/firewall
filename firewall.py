#!/bin/env python3
import argparse
import configparser
import getpass
import ipaddress
import json
import logging
import os
import subprocess
import sys

import jinja2
import ldap

import re2oapi


path = os.path.dirname(os.path.abspath(__file__))

logger = logging.getLogger('firewall')
handler = logging.StreamHandler(sys.stderr)
formatter = logging.Formatter('%(asctime)s - %(name)s[%(process)d] - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)


def nat_map(private_subnet, public_subnet):
    """
    Divise private_subnet en subnets de même taille et map ces subnets derrière les ip de public_subnet
    """
    nat_subnets = [ nat_subnet for nat_subnet in private_subnet.subnets(prefixlen_diff=public_subnet.num_addresses.bit_length()-1) ]
    return list(zip(nat_subnets, public_subnet))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Generate firewall from LDAP",
    )
    parser.add_argument("-e", "--export", help="Exporte le contenu des pare-feu sur la sortie standard", action="store_true")
    parser.add_argument("-l", "--ldap-server", help="URL de la base ldap à contacter", type=str, default=None)
    parser.add_argument("-p", "--re2o-password", help="Demande le mot de passe de l'utilisateur re2o", action="store_true")
    parser.add_argument("-q", "--quiet", help="Diminue la verbosité des logs (à spécifier plusieurs fois pour diminuer la verbosité)", action='count', default=0)
    parser.add_argument("-r", "--re2o-server", help="Nom du serveur re2o à contacter", type=str, default=None)
    parser.add_argument("-u", "--re2o-user", help="Utilisateur re2o", type=str, default=None)
    parser.add_argument("-v", "--verbose", help="Augmente la verbosité des logs (à spécifier plusieurs fois pour augmenter la verbosité)", action='count', default=0)
    args = parser.parse_args()

    verbosity = args.verbose - args.quiet

    if verbosity <= -1:
        logger.setLevel(logging.WARNING)
    elif verbosity == 0:
        logger.setLevel(logging.INFO)
    elif verbosity >= 1:
        logger.setLevel(logging.DEBUG)

    logger.info("Reading configuration")
    with open(os.path.join(path, "firewall.json")) as config_file:
        config = json.load(config_file)
    logger.debug("Loaded {}".format(config))

    if args.ldap_server is not None:
        config['ldap_url'] = args.ldap_server

    logger.info("Connecting to LDAP")
    base = ldap.initialize(config['ldap_url'])
    if config['ldap_url'].startswith('ldaps://'):
        # On ne vérifie pas le certificat pour le LDAPS
        logger.debug("Using LDAPS: changing TLS context")
        base.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_ALLOW)
        base.set_option(ldap.OPT_X_TLS_NEWCTX, 0)

    logger.info("Querying LDAP")
    hosts_query_id = base.search("ou=hosts,dc=crans,dc=org", ldap.SCOPE_SUBTREE, "objectClass=ipHost")
    hosts_query = base.result(hosts_query_id)
    services_query_id = base.search("ou=services,dc=crans,dc=org", ldap.SCOPE_SUBTREE, "objectClass=ipService")
    services_query = base.result(services_query_id)
    networks_query_id = base.search("ou=networks,dc=crans,dc=org", ldap.SCOPE_SUBTREE, "objectClass=ipNetwork")
    networks_query = base.result(networks_query_id)

    networks = {}

    # On récupère les subnets depuis la base LDAP, en particulier les subnet natés pour la génération du nat
    for dn, entry in networks_query[1]:
        networks[entry['cn'][0].decode('utf-8')] = ipaddress.ip_network(entry['ipNetworkNumber'][0].decode('utf-8') + '/' + entry['ipNetmaskNumber'][0].decode('utf-8'))

    logger.debug("Queried {} networks: {}".format(len(networks), networks))

    services = {}

    for dn, entry in services_query[1]:
        if 'description' in entry:
            # C'est une range de port, le port final est dans la description du service
            ports = (int(entry['ipServicePort'][0]), int(entry['description'][0]))
        else:
            port = int(entry['ipServicePort'][0])
            ports = (port, port)
        protocols = { protocol.decode('utf-8') for protocol in entry['ipServiceProtocol'] }
        services[entry['cn'][0].decode('utf-8')] = (ports, protocols)

    logger.debug("Queried {} services: {}".format(len(services), services))

    ports_openings = []

    for dn, entry in hosts_query[1]:
        domain = dn.split(',', 1)[0]
        domain = domain.split('.')
        if len(domain) == 3:
            subnet = 'srv'
        elif len(domain) == 4:
            subnet = domain[1]
        if 'description' not in entry:
            # Pas d'ouverture de port : on utilise l'ouverture par défaut du subnet
            if subnet in config['DEFAULT_SERVICES']:
                opening = config['DEFAULT_SERVICES'][subnet]
            else:
                continue
        else:
            opening = entry['description'][0].decode('utf-8').strip()
            opening = opening.split(',')
            opening = [ tuple(open.split(':')) for open in opening ]
            if subnet in config['DEFAULT_SERVICES']:
                opening += tuple(config['DEFAULT_SERVICES'][subnet])
        ip_addresses = [ipaddress.ip_address(ip.decode('utf-8')) for ip in entry['ipHostNumber']]
        opening_in = { open[1] for open in opening if open[0] == 'in' }
        opening_out = { open[1] for open in opening if open[0] == 'out' }
        for ip in ip_addresses:
            tcp_ports_in = [ services[service][0] for service in opening_in if 'tcp' in services[service][1] ]
            tcp_ports_in.sort()
            tcp_ports_in = ','.join( '{}-{}'.format(port[0], port[1]) if port[0] != port[1] else str(port[0]) for port in tcp_ports_in )
            if tcp_ports_in:
                ports_openings.append('ip{ip_version} daddr {ip} tcp dport {{ {ports} }} accept'.format(ip_version='' if ip.version == 4 else '6', ip=ip, ports=tcp_ports_in))
            udp_ports_in = [ services[service][0] for service in opening_in if 'udp' in services[service][1] ]
            udp_ports_in.sort()
            udp_ports_in = ','.join( '{}-{}'.format(port[0], port[1]) if port[0] != port[1] else str(port[0]) for port in udp_ports_in )
            if udp_ports_in:
                ports_openings.append('ip{ip_version} daddr {ip} udp dport {{ {ports} }} accept'.format(ip_version='' if ip.version == 4 else '6', ip=ip, ports=udp_ports_in))
            tcp_ports_out = [ services[service][0] for service in opening_out if 'tcp' in services[service][1] ]
            tcp_ports_out.sort()
            tcp_ports_out = ','.join( '{}-{}'.format(port[0], port[1]) if port[0] != port[1] else str(port[0]) for port in tcp_ports_out )
            if tcp_ports_out:
                ports_openings.append('ip{ip_version} saddr {ip} tcp dport {{ {ports} }} accept'.format(ip_version='' if ip.version == 4 else '6', ip=ip, ports=tcp_ports_out))
            udp_ports_out = [ services[service][0] for service in opening_out if 'udp' in services[service][1] ]
            udp_ports_out.sort()
            udp_ports_out = ','.join( '{}-{}'.format(port[0], port[1]) if port[0] != port[1] else str(port[0]) for port in udp_ports_out )
            if udp_ports_out:
                ports_openings.append('ip{ip_version} saddr {ip} udp dport {{ {ports} }} accept'.format(ip_version='' if ip.version == 4 else '6', ip=ip, ports=udp_ports_out))

    logger.debug("Generated {} ports openings".format(len(ports_openings)))

    logger.info("Reading Re2o configuration")
    re2o_config = configparser.ConfigParser()
    re2o_config.read(os.path.join(path, 're2o-config.ini'))

    if args.re2o_server is not None:
        api_hostname = args.re2o_server
    else:
        api_hostname = re2o_config.get('Re2o', 'hostname')
    if args.re2o_user is not None:
        api_username = args.re2o_user
    else:
        api_username = re2o_config.get('Re2o', 'username')
    if args.re2o_password:
        api_password = getpass.getpass('Re2o password: ')
    else:
        api_password = re2o_config.get('Re2o', 'password')

    logger.info("Connecting to Re2o")
    api_client = re2oapi.Re2oAPIClient(api_hostname, api_username, api_password, use_tls=False)

    logger.info("Querying Re2o")
    interface_ports = api_client.list("firewall/interface-ports/")
    ports_openings_adh = [] # les ouvertures de ports des serveurs des adhérents

    for interface in interface_ports:
        for ip in [ ipaddress.ip_address(interface['ipv4']) ] + [ ipaddress.ip_address(ipv6['ipv6']) for ipv6 in interface['ipv6'] ]:
            tcp_ports_in = set()
            tcp_ports_out = set()
            udp_ports_in = set()
            udp_ports_out = set()
            for subnet in config['DEFAULT_SERVICES_RE2O']:
                if ip in ipaddress.ip_network(subnet):
                    if 'tcp_in' in config['DEFAULT_SERVICES_RE2O'][subnet]:
                        for opening in config['DEFAULT_SERVICES_RE2O'][subnet]['tcp_in']:
                            tcp_ports_in.add(tuple(opening))
            for opening in interface['port_lists']:
                for port_range in opening['tcp_ports_in']:
                    tcp_ports_in.add((port_range['begin'], port_range['end']))
            if tcp_ports_in:
                tcp_ports_in = ','.join( '{}-{}'.format(port[0], port[1]) if port[0] != port[1] else str(port[0]) for port in sorted(tcp_ports_in) )
                ports_openings_adh.append('ip{ip_version} daddr {ip} tcp dport {{ {ports} }} accept'.format(ip_version='' if ip.version == 4 else '6', ip=ip, ports=tcp_ports_in))
            for opening in interface['port_lists']:
                for port_range in opening['tcp_ports_out']:
                    tcp_ports_out.add((port_range['begin'], port_range['end']))
            if tcp_ports_out:
                tcp_ports_out = ','.join( '{}-{}'.format(port[0], port[1]) if port[0] != port[1] else str(port[0]) for port in sorted(tcp_ports_out) )
                ports_openings_adh.append('ip{ip_version} saddr {ip} tcp dport {{ {ports} }} accept'.format(ip_version='' if ip.version == 4 else '6', ip=ip, ports=tcp_ports_out))
            for opening in interface['port_lists']:
                for port_range in opening['udp_ports_in']:
                    udp_ports_in.add((port_range['begin'], port_range['end']))
            if udp_ports_in:
                udp_ports_in = ','.join( '{}-{}'.format(port[0], port[1]) if port[0] != port[1] else str(port[0]) for port in sorted(udp_ports_in) )
                ports_openings_adh.append('ip{ip_version} daddr {ip} udp dport {{ {ports} }} accept'.format(ip_version='' if ip.version == 4 else '6', ip=ip, ports=udp_ports_in))
            for opening in interface['port_lists']:
                for port_range in opening['udp_ports_out']:
                    udp_ports_out.add((port_range['begin'], port_range['end']))
            if udp_ports_out:
                udp_ports_out = ','.join( '{}-{}'.format(port[0], port[1]) if port[0] != port[1] else str(port[0]) for port in sorted(udp_ports_out) )
                ports_openings_adh.append('ip{ip_version} saddr {ip} udp dport {{ {ports} }} accept'.format(ip_version='' if ip.version == 4 else '6', ip=ip, ports=udp_ports_out))

    logger.debug("Generated {} ports opening".format(len(ports_openings_adh)))

    with open(os.path.join(path, 'templates', 'nftables.conf.j2')) as firewall_template:
        template = jinja2.Template(firewall_template.read())

    logger.info("Generating NAT")
    nat = { subnet: nat_map(networks[subnet], ipaddress.ip_network(config['NAT'][subnet])) for subnet in config['NAT'] }

    if args.export:
        print(template.render(nat=nat, nat_interface=config['nat_interface'], ports_openings=ports_openings, ports_openings_adh=ports_openings_adh))
    else:
        with open('/etc/nftables.conf', 'w') as nftables:
            nftables.write(template.render(nat=nat, nat_interface=config['nat_interface'], ports_openings=ports_openings, ports_openings_adh=ports_openings_adh))
        subprocess.run(['systemctl', 'reload', 'nftables'])
